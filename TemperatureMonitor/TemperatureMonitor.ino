#include <ESP8266WiFi.h>
#include <FirebaseArduino.h>

#define FIREBASE_HOST "hacktoon-tempmonitor-default-rtdb.firebaseio.com"
#define FIREBASE_AUTH "cY6ENjDyohpUWwMsYV909Bj8SJqkTNo4eqpFKHGZ"
#define WIFI_SSID "SherlockHolmes"
#define WIFI_PASSWORD "SherLock@Holmes"

int outputpin = A0;
int ledPin = D1;

void setup() {
  Serial.begin(9600);
  delay(1000);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);  
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  Serial.print("Connecting to ");
  Serial.print(WIFI_SSID);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.print("Connected to ");
  Serial.println(WIFI_SSID);
  Serial.print("IP Address is : ");
  Serial.println(WiFi.localIP());
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, 1);
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
}

void loop() {
  int analogValue = analogRead(outputpin);
  float millivolts = (analogValue/1024.0) * 3300; //3300 is the voltage provided by NodeMCU
  float celsius = millivolts/10;
  Firebase.setFloat("temp", celsius);
  if (Firebase.failed()) {  
      Serial.print("setting /number failed:");  
      Serial.println(Firebase.error());    
      return;  
  }
  digitalWrite(ledPin, Firebase.getInt("running"));
  Serial.print("Current Temperature in Degree Celsius =   ");
  Serial.println(celsius);
}
